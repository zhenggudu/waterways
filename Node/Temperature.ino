// 打印温度
void getTemperature() {
  // String strt = "sensors is : ";
  // 请求读取传感器温度
  sensors.requestTemperatures();
  // 查询读取温度所需时间（毫秒）
  // int conversionTime = sensors.millisToWaitForConversion(sensors.getResolution());
  // 等待温度读取完成
  // wait(conversionTime);
  // 遍历所有温度传感器
  for (int i=0; i<tempCount; i++){
    // 输出地址
    // strt += i;
    // strt += "-";
    float t = sensors.getTempC(dAddress[i]);
    // 判断读取温度是否正确
    if (t != DEVICE_DISCONNECTED_C) {
      TempC[i] = t;
      // send(msgt.setSensor(CHILD_ID_T + i).set(t,2), true);
      // strt += t;
    } else {
      // strt += "error";
    }
    // strt += ";";
  }
  // Serial.println(strt);
}

// 打印温度传感器地址
String printAddress(DeviceAddress deviceAddress)
{
  String str;
  for (uint8_t i = 0; i < 8; i++)
  {
    // zero pad the address if necessary
    if (deviceAddress[i] < 16) str += "0";
    str += deviceAddress[i];
  }
  return str;
}
