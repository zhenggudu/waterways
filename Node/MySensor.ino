void presentation()
{
  // 设置当前传感器模组信息
  sendSketchInfo("XunHuan Sensor", "1.0");

  // 初始化传感器
  present(CHILD_ID_XH_ONOFF, S_MOTION, "sensor onoff");
  present(CHILD_ID_XH_STATE, S_BINARY, "sensor state");
  present(CHILD_ID_XH_SLAVE, S_BINARY, "sensor slave");
  present(CHILD_ID_XH_ERROR, S_INFO, "sensor error");
  present(CHILD_ID_XH_FLOW, S_WATER, "sensor flow");

  // 温度传感器
  for (int i = 0; i < TEMP_COUNT_MAX; i++) {
    // 当获取不到地址是跳出循环
    if (!sensors.getAddress(dAddress[i], i)) {
      break;
    } else {
      tempCount ++;
    }
    // 设置传感器信息
    String info = "sensor t NO.";
    info += i + 1;
    char infos[20];
    info.toCharArray(infos, 20);
    present(CHILD_ID_TEMP + i, S_TEMP, infos);
  }
}

void receive(const MyMessage &message) {
  // 是否时确认消息
  bool isAck = message.isAck();

  /*
    // 输出测试参数
    String ms = "receive is:";
    ms += message.type;
    ms += ";";
    // 获取请求传感器nodeid
    ms += message.sender;
    ms += "-";
    // 发送方设置的传感器子id
    ms += message.sensor;
    ms += ";";
    // 目标传感器nodeid
    ms += message.destination;
    ms += ";";
    if (isAck) {
    ms += " is Ack;";
    }
    Serial.println(ms);
  */

  // 设定开关状态
  if (message.sensor == CHILD_ID_XH_ONOFF && message.type == V_ARMED && !isAck) {
    updateOnOff(message.getInt());
  } else if (message.type == V_STATUS && !isAck) { // 当输入值为二进制状态值时
    // 获取状态值
    int value = message.getInt();
    // 根据id设置不同参数
    switch (message.sensor) {
      case CHILD_ID_XH_STATE:
        updateState(value);
        break;
      case CHILD_ID_XH_SLAVE:
        updateSlave(value);
        break;
      default:
        break;
    }
  }
}

// 设置启动参数
void updateOnOff(int state) {
  if (oldvalueOnOff != state) {
    oldvalueOnOff = state;
    send(msgOnOff.set(state));
  }
}

// 设置工作模式
void updateState(int state) {
  if (oldvalueState != state) {
    oldvalueState = state;
    send(msgState.set(state));
  }
}

// 触发工作状态
void updateTrigger(int state) {
  if (oldvalueTrigger != state) {
    oldvalueTrigger = state;
    send(msgTrigger.set(state));
  }
}

// 是否启用副热水器
void updateSlave(int state) {
  if (oldvalueSlave != state) {
    oldvalueSlave = state;
    // 设置主副水路标志
    if (state == ON) {
      mastOrSlave = false;
    } else {
      mastOrSlave = true;
    }
    send(msgSlave.set(state));
  }
}

// 更新错误状态
void updateError(int state) {
  if (oldvalueError != state) {
    oldvalueError = state;
    send(msgError.set(state));
  }
}

// 更新流量
void updateFlow() {
  send(msgFlow.set(flowVal, 2));
  send(msgVolume.set(volumeVal, 2));
}

// 更新温度
void updateTemp() {
  for (int i = 0; i < tempCount; i++) {
    // 过滤不正确的初始温度
    if (TempC[i] != 85.00 && TempC[i] != -127.00) {
      send(msgTemp.setSensor(CHILD_ID_TEMP + i).set(TempC[i], 2));
    }
  }
}
