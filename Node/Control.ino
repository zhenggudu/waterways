// 打开
void OnControl() {
  //主热水器电源打开
  WaterHeaterMasterPower(ON);
  // 当副热水器启用时
  if (oldvalueSlave == ON) {
    // 启动副热水器电源
    WaterHeaterSlavePower(ON);
    // 根据启用的水路设置球阀开关
    if (mastOrSlave) {
      WaterHeaterMaster(ON);
      WaterHeaterSlave(OFF);
    } else {
      WaterHeaterMaster(OFF);
      WaterHeaterSlave(ON);
    }
  } else {
    // 副热水器关闭
    WaterHeaterMaster(ON);
    WaterHeaterSlave(OFF);
    WaterHeaterSlavePower(OFF);
  }
}

// 关闭设备
void OffControl() {
  WaterPump(OFF);
  WaterHeaterMaster(OFF);
  WaterHeaterMasterPower(OFF);
  WaterHeaterSlave(OFF);
  WaterHeaterSlavePower(OFF);
}

void errorControl(int errror) {
  // 返回错误
  updateError(errror);
  // 转换到触发模式
  updateState(OFF);
  // 关闭循环
  StopXH();
  // 设置错误延迟
  errorTime = millis();
}

// 错误等待
bool ErrorWait(){
  // 当没有错误或者不需要等待时返回true
  return oldvalueError == 0 || isWait(errorTime, ErrorTimeSpan * 1000);
}

// 启动循环
void StartXH() {
  // 启动水泵
  WaterPump(ON);
  updateTrigger(ON);
  // 设置循环时间戳
  startTime = millis();
  startLongTime = millis();
}

// 关闭循环
void StopXH() {
  WaterPump(OFF);
  updateTrigger(OFF);
}

// 启停水泵
void WaterPump(int state) {
  if (oldvalueWP != state) {
    oldvalueWP = state;
    digitalWrite(WP_PIN, state == HightOrLow ? HIGH : LOW);
  }
}

// 主热水器
void WaterHeaterMaster(int state) {
  if (oldvalueWHM != state) {
    oldvalueWHM = state;
    digitalWrite(WHM_PIN, state == HightOrLow ? HIGH : LOW);
  }
}

// 主热水器电源电源
void WaterHeaterMasterPower(int state) {
  if (oldvalueWHMP != state) {
    oldvalueWHMP = state;
    digitalWrite(WHM_POWER_PIN, state == HightOrLow ? HIGH : LOW);
  }
}

// 副热水器
void WaterHeaterSlave(int state) {
  if (oldvalueWHS != state) {
    oldvalueWHS = state;
    digitalWrite(WHS_PIN, state == HightOrLow ? HIGH : LOW);
  }
}

// 副热水器电源
void WaterHeaterSlavePower(int state) {
  if (oldvalueWHSP != state) {
    oldvalueWHSP = state;
    digitalWrite(WHS_POWER_PIN, state == HightOrLow ? HIGH : LOW);
  }
}
