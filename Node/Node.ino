/*
   当前为传感器节点模块
*/

// Enable debug prints to serial monitor
#define MY_DEBUG

// Enable RS485 transport layer
#define MY_RS485

// Define this to enables DE-pin management on defined pin
#define MY_RS485_DE_PIN 2

// Set RS485 baud rate to use
#define MY_RS485_BAUD_RATE 9600

// Enable this if RS485 is connected to a hardware serial port
//#define MY_RS485_HWSERIAL Serial1

// 传感器id
#define MY_NODE_ID 40

#include <MySensors.h>
#include <MsTimer2.h>
// 温度传感器读取库
#include <OneWire.h>
#include <DallasTemperature.h>

// 传感器id
#define CHILD_ID_XH_ONOFF 41
#define CHILD_ID_XH_STATE 42
#define CHILD_ID_XH_SLAVE 43
#define CHILD_ID_XH_ERROR 44
// 流量传感器
#define CHILD_ID_XH_FLOW 45
// 设置温度传感器起始id
#define CHILD_ID_TEMP 46
#define TEMP_COUNT_MAX 3
// 热水器温度传感器索引id（不能大于最大传感器数，从0开始记数）
#define WH_TEMP_INDEX_MASTER 0
#define WH_TEMP_INDEX_SLAVE 1
#define BW_TEMP_INDEX 2

// 开关状态
#define ON 1
#define OFF 0
// 输出控制为，高电频触发或低电平触发(1为高电平，0为低电平)
#define HightOrLow 1

// 设定启动文温度，当温度低于指定值时启动
#define StartTemp 30
// 设定停止温度，当温度高于该值则停止
#define StopTemp 35
// 设定出水检查温度
#define OutWaterTemp 30
// 设定出水检查延迟
#define OutWaterTime 15
// 错误等待延迟
#define ErrorTimeSpan 10
// 副循环恢复延迟
#define OpenSlaveSpan 7200
// 霍尔流量计常数（不能为0）
#define FlowQ 8

// 温度传感器引脚
#define ONE_WIRE_BUS 10
// 流量中断引脚
#define INPUT_PIN 3

// 控制引脚
// 水泵
#define WP_PIN 14
// 主热水器阀门
#define WHM_PIN 17
// 副热水器阀门
#define WHS_PIN 18
// 主热水器电源
#define WHM_POWER_PIN 15
// 副热水器电源
#define WHS_POWER_PIN 16

// 初始化温度传感器读取
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

// 初始化消息发送函数
// 开关，运行模式，触发
MyMessage msgOnOff(CHILD_ID_XH_ONOFF, V_ARMED);
MyMessage msgTrigger(CHILD_ID_XH_ONOFF, V_TRIPPED);
MyMessage msgState(CHILD_ID_XH_STATE, V_STATUS);
//副热水器，和报错
MyMessage msgSlave(CHILD_ID_XH_SLAVE, V_STATUS);
MyMessage msgError(CHILD_ID_XH_ERROR, V_TEXT);
// 以下为传感器
MyMessage msgFlow(CHILD_ID_XH_FLOW, V_FLOW);
MyMessage msgVolume(CHILD_ID_XH_FLOW, V_VOLUME);
MyMessage msgTemp(CHILD_ID_TEMP, V_TEMP);

// 温度传感器地址和暂存的温度
DeviceAddress dAddress[TEMP_COUNT_MAX];
float TempC[TEMP_COUNT_MAX];
int tempCount = 0;

// 霍尔流量计记数器
int numVal = 0;
//霍尔流量计输出数据
float flowVal = 0;
float volumeVal = 0;

// 状态记录
// 开关状态
int oldvalueOnOff = -1;
// 运行触发
int oldvalueTrigger = -1;
// 运行模式off为触发模式On为恒温
int oldvalueState = -1;
// 副热水器是否启用off为关，on为启用
int oldvalueSlave = -1;
// 错误消息
/*
   错误代码：
   -1：初始状态
   0：正常
   1: 热水器出水温度不达标，请检查热水器是否开启
   2：水泵运转，但是流量计不运转，检查管路中是否有水或管路是否堵塞
   3: 循环泵长时间运行，且回水温度不达标
*/
int oldvalueError = -1;

// 主副水路标志
bool mastOrSlave = true;

// 控制引脚状态记录
// 水泵
int oldvalueWP = -1;
// 主热水器阀门
int oldvalueWHM = -1;
// 副热水器阀门
int oldvalueWHS = -1;
// 主热水器电源
int oldvalueWHMP = -1;
// 副热水器电源
int oldvalueWHSP = -1;

// 延迟时间戳
// 传感器输出延时
unsigned long outputTime = 0;
// 主循环操作延时
unsigned long loopTime = 0;
// 循环延时
unsigned long startTime = 0;
// 防止水泵运行时间过长
unsigned long startLongTime = 0;
// 副循环恢复延迟
unsigned long openSlaveTime = 0;
// 出现错误后延迟
unsigned long errorTime = 0;

void setup() {
  Serial.begin(115200);

  // 设置霍尔中断输入
  attachInterrupt(digitalPinToInterrupt(INPUT_PIN), count, FALLING);

  // 设置内部中断
  // 每1秒出发
  MsTimer2::set(1000, flash);
  //开始计时
  MsTimer2::start();

  // 设置控制引脚
  pinMode(WP_PIN, OUTPUT);
  pinMode(WHM_PIN, OUTPUT);
  pinMode(WHS_PIN, OUTPUT);
  pinMode(WHM_POWER_PIN, OUTPUT);
  pinMode(WHS_POWER_PIN, OUTPUT);
  
  // 设置默认状态
  OffControl();

  
  // 单独测试
  updateOnOff(ON);
  updateState(OFF);
  updateTrigger(OFF);
  updateSlave(OFF);

  // 初始化控制参数
  // updateOnOff(OFF);
  // updateState(OFF);
  // updateTrigger(OFF);
  // updateSlave(OFF);
  updateError(0);

  // 获取最初的传感器温度
  getTemperature();
}

// 霍尔流量计脉冲输入
void count() {
  numVal++;
}

// 每秒定时触发
void flash() {
  //  每秒刷新霍尔流量计
  if (numVal > 0) {
    flowVal = float(numVal) / FlowQ;
  } else {
    flowVal = 0;
  }
  // 累加流量
  volumeVal += flowVal;
  // 重置脉冲数
  numVal = 0;

  // 刷新温度传感器
  getTemperature();
}

void loop() {
  // 等待执行一次输出传感器数值
  if (isWait(outputTime, 5000)) {
    // 更新时间戳
    outputTime = millis();
    // 输出流量数值
    updateFlow();
    // 输出温度传感器温度
    updateTemp();
  }

  // 等待执行一次主操作
  if (isWait(loopTime, 500)) {
    // 更新时间戳
    loopTime = millis();

    // 总开关打开
    if (oldvalueOnOff == ON) {
      // 启用设备
      OnControl();
      // 恒温模式，出现错误则等待10秒
      if (oldvalueState == ON && ErrorWait()) {
        // 温度不达标则启动
        if (TempC[BW_TEMP_INDEX] < StartTemp && oldvalueTrigger == OFF) {
          // 将错误移除
          updateError(0);
          // 启动循环
          StartXH();
          // Serial.println("hengweng Start");
        } else if (TempC[BW_TEMP_INDEX] > StopTemp && oldvalueTrigger == ON) { //温度达标
          // 关闭循环
          StopXH();
          // Serial.println("hengweng Stop");
        }
      } else if (ErrorWait()) {// 触发模式，出现错误则等待10秒
        // 有流动，并且温度不达标则启动
        if (flowVal > 1 && TempC[BW_TEMP_INDEX] < StartTemp && oldvalueTrigger == OFF) {
          // 将错误移除
          updateError(0);
          // 启动循环
          StartXH();
          // Serial.println("chufa Start");
        } else if (TempC[BW_TEMP_INDEX] > StopTemp && oldvalueTrigger == ON) { // 温度达标
          // 关闭循环
          StopXH();
          // Serial.println("chufa Stop");
        }
      }
    } else {
      // 关闭设备
      OffControl();
      //关闭循环
      if (oldvalueTrigger == ON) {
        StopXH();
      }
    }
  }

  // 20秒循环，热水器启动检查
  if (isWait(startTime, OutWaterTime * 1000) && oldvalueTrigger == ON) {
    startTime = millis();
    // 检查运行状态，防止无水时水泵运行
    if (flowVal < 1) {
      // 停止运行，并返回错误
      errorControl(2);
    } else if (oldvalueSlave == ON && !mastOrSlave && TempC[WH_TEMP_INDEX_SLAVE] < OutWaterTemp) {
      // 当副热水器启用时,并且是副循环时,出水温度不达标
      //启用主循环
      mastOrSlave = true;
      // 设置副循环恢复时间戳
      openSlaveTime = millis();
      // Serial.println("Open Master");
    } else if ((oldvalueSlave == ON && mastOrSlave || oldvalueSlave == OFF) && TempC[WH_TEMP_INDEX_MASTER] < OutWaterTemp) {
      // 当前副热水器启动，并且为主循环时，或副热水器关闭，出水温度不达标
      errorControl(1);
      // Serial.println("chushui NO");
    }
  }

  // 延时恢复副循环
  // 当前副热水器启动，并且启动了主循环
  if (oldvalueSlave == ON && mastOrSlave && isWait(openSlaveTime, OpenSlaveSpan * 1000)) {
    openSlaveTime = millis();
    mastOrSlave = false;
  }

  // 30分钟延迟，热水器运行时间过长检查
  if (oldvalueTrigger == ON && isWait(startLongTime, 3600 * 1000)) {
    startLongTime = millis();
    // 检查回水水温
    if (TempC[BW_TEMP_INDEX] < StopTemp) {
      // 停止并返回错误
      errorControl(3);
    }
  }

}

// 是否到等待时间
bool isWait(unsigned long sTime, unsigned long wTime) {
  // 获取当前时间
  unsigned long now = millis();
  unsigned long dTime = now - sTime;
  // 当间隔时间大于设置等待时间时返回true
  return dTime > wTime;
}
